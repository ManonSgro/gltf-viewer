#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const{

  std::vector<GLuint> textureObjects(model.textures.size(), 0);

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  glActiveTexture(GL_TEXTURE0); // it should be texture unit 0 since we did not call glActiveTexture before

  // generate all texture objects
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());

  for (size_t i = 0; i < model.textures.size(); ++i) {
    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0); // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image

    const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);

    // Set image data:
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());

    // Set sampling parameters:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    // Set wrapping parameters:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }

  // debind
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
}

bool ViewerApplication::loadGltfFile(tinygltf::Model & model){
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF\n");
    return -1;
  }
  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects( const tinygltf::Model &model){
  
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0);
  glGenBuffers(model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }
  glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects( const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> & meshIndexToVaoRange){
  std::vector<GLuint> vertexArrayObjects;

  // For each mesh of model we keep its range of VAOs
  meshIndexToVaoRange.resize(model.meshes.size());

  const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
  const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
  const GLuint VERTEX_ATTRIB_TEXCOORD_0_IDX = 2;

  for(int meshIdx=0; meshIdx<model.meshes.size(); ++meshIdx){

    // save the value
    auto &vaoRange = meshIndexToVaoRange[meshIdx];
    vaoRange.begin = GLsizei(vertexArrayObjects.size());
    vaoRange.count = GLsizei(model.meshes[meshIdx].primitives.size());

    const auto vaoOffset = vertexArrayObjects.size();    
    vertexArrayObjects.resize(vaoOffset + model.meshes[meshIdx].primitives.size());
    //meshIndexToVaoRange.push_back(VaoRange{vaoOffset, model.meshes[meshIdx].primitives.size()});
    glGenVertexArrays(vaoRange.count, &vertexArrayObjects[vaoOffset]);

    for(int primitiveIdx = 0; primitiveIdx < model.meshes[meshIdx].primitives.size(); ++primitiveIdx){
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);
      {
      const auto iterator = model.meshes[meshIdx].primitives[primitiveIdx].attributes.find("POSITION");
      if (iterator != end(model.meshes[meshIdx].primitives[primitiveIdx].attributes)) {
        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        const auto bufferObject = model.buffers[bufferIdx];

        // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
        glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);
                
        // TODO Bind the buffer object to GL_ARRAY_BUFFER
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
        // TODO Call glVertexAttribPointer with the correct arguments.
        glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX,
          accessor.type,
          accessor.componentType,
          GL_FALSE,
          GLsizei(bufferView.byteStride),
          (const GLvoid*)byteOffset);        
      }
      }

      {
      const auto iterator = model.meshes[meshIdx].primitives[primitiveIdx].attributes.find("NORMAL");
      if (iterator != end(model.meshes[meshIdx].primitives[primitiveIdx].attributes)) {
        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        const auto bufferObject = model.buffers[bufferIdx];

        // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
        glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);
                
        // TODO Bind the buffer object to GL_ARRAY_BUFFER
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
        // TODO Call glVertexAttribPointer with the correct arguments.
        glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX,
          accessor.type,
          accessor.componentType,
          GL_FALSE,
          GLsizei(bufferView.byteStride),
          (const GLvoid*)byteOffset);        
      }
      }

      {
      const auto iterator = model.meshes[meshIdx].primitives[primitiveIdx].attributes.find("TEXCOORD_0");
      if (iterator != end(model.meshes[meshIdx].primitives[primitiveIdx].attributes)) {
        const auto accessorIdx = (*iterator).second;
        const auto &accessor = model.accessors[accessorIdx];
        const auto &bufferView = model.bufferViews[accessor.bufferView];
        const auto bufferIdx = bufferView.buffer;

        const auto bufferObject = model.buffers[bufferIdx];

        // TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
        glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD_0_IDX);
                
        // TODO Bind the buffer object to GL_ARRAY_BUFFER
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[bufferIdx]);

        const auto byteOffset = accessor.byteOffset + bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
        // TODO Call glVertexAttribPointer with the correct arguments.
        glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD_0_IDX,
          accessor.type,
          accessor.componentType,
          GL_FALSE,
          GLsizei(bufferView.byteStride),
          (const GLvoid*)byteOffset);        
      }
    }

    if(model.meshes[meshIdx].primitives[primitiveIdx].indices >= 0){
      // Get buffer id
      const auto accessorIdx = model.meshes[meshIdx].primitives[primitiveIdx].indices;
      const auto &accessor = model.accessors[accessorIdx];
      const auto &bufferView = model.bufferViews[accessor.bufferView];
      const auto bufferIdx = bufferView.buffer;

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
    }

    }

  }

  glBindVertexArray(0); // unbind
  return vertexArrayObjects;
}

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram =
      compileProgram({m_ShadersRootPath / m_vertexShader,
          m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uLightingDirection =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto uLightIntensity =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");
  const auto uBaseColorTexture =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto uMetallicFactor =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactor =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uMetallicRoughnessTexture =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uEmissiveTexture =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uOcclusionTexture =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionFactor =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionFactor");
  const auto uApplyOcclusion =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");


  glm::vec3 lightDirection = glm::vec3(1,1,1), lightIntensity = glm::vec3(1,1,1);
  bool lightFromCamera = false; // global
  bool applyOcclusion = true;

  tinygltf::Model model;
  // TODO Loading the glTF file
  loadGltfFile(model);

  // Bounding box
  // formula for the center point is (max+min)/2
  // formula for the diagonal vector is min-max
  // condition to check if a scene is flat on the z axis : if 2 points have the same z value
  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  // Build projection matrix
  //auto maxDistance = 500.f; // TODO use scene bounds instead to compute this
  //maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  const auto diagonalVector = bboxMax-bboxMin;
  const float maxDistance = ( glm::length(diagonalVector)>0 ? glm::length(diagonalVector) : 100.0f );
  const float near = 0.001f * maxDistance;
  const float far = 1.5f * maxDistance;
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          near, far);
  

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  /*
  FirstPersonCameraController cameraController{
      m_GLFWHandle.window(), 1.0f * maxDistance};
  TrackballCameraController cameraController{
      m_GLFWHandle.window(), 1.0f * maxDistance};
      */
  std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
  
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    //cameraController.setCamera(Camera{glm::vec3(0, 0, 0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)});
    const glm::vec3 camCenter = glm::vec3((bboxMin.x+bboxMax.x)/2,(bboxMin.y+bboxMax.y)/2,(bboxMin.z+bboxMax.z)/2);
    std::cout<<"UP :"<<m_upVector<<std::endl;
    //const auto camUp = glm::vec3(0,1,0);
    const auto camUp = ( m_upVector=="y" ? glm::vec3(0,1,0) : glm::vec3(0,0,1) );
    const auto camEye = ( diagonalVector.z<=0 ? camCenter + 2.f * glm::cross(diagonalVector, camUp) : camCenter+diagonalVector);
    cameraController->setCamera(Camera{camEye, camCenter, camUp});
  }

  // TEXTURES
  // Load textures
  const auto textureObjects = createTextureObjects(model);

  // White base texture
  GLuint whiteTexture = 0;

  glGenTextures(1, &whiteTexture);

  // bind
  glBindTexture(GL_TEXTURE_2D, whiteTexture);

  float white[] = {1, 1, 1, 1};

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);

  // sampling parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // wrapping parameters
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  // debind
  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  const std::vector<GLuint> bufferObjects = createBufferObjects(model);
 
  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange; // declare variable
  const std::vector<GLuint> vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);


  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  
  const auto bindMaterial = [&](const auto materialIndex) {
    // Material binding
    if(materialIndex >= 0){ // if materialIndex >= 0 
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
      if (uBaseColorTexture >= 0) { // if this material has a base color
        auto textureObject = whiteTexture; // bind white material to prevent errors
        if(pbrMetallicRoughness.baseColorTexture.index >= 0){
          const auto &texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
          assert(texture.source >= 0); // ensure a source image is present
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }
        
        // Bind
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uBaseColorFactor >= 0) { // if this material has a base color
        glUniform4f(uBaseColorFactor,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      // Metallic roughness
      if (uMetallicFactor >= 0) {
        glUniform1f(
            uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(
            uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);
      }
      if (uMetallicRoughnessTexture >= 0) {
        auto textureObject = 0u;
        if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
          const auto &texture =
              model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uMetallicRoughnessTexture, 1); // bind on 1
      }

      // Emissive
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0],
            (float)material.emissiveFactor[1],
            (float)material.emissiveFactor[2]);
      }
      if (uEmissiveTexture >= 0) {
        auto textureObject = 0u;
        if (material.emissiveTexture.index >= 0) {
          const auto &texture = model.textures[material.emissiveTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uEmissiveTexture, 2);
      }

      // Occlusion
      if (uOcclusionFactor >= 0) {
        glUniform1f(
            uOcclusionFactor, (float)material.occlusionTexture.strength);
      }
      if (uOcclusionTexture >= 0) {
        auto textureObject = whiteTexture;
        if (material.occlusionTexture.index >= 0) {
          const auto &texture = model.textures[material.occlusionTexture.index];
          if (texture.source >= 0) {
            textureObject = textureObjects[texture.source];
          }
        }

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, textureObject);
        glUniform1i(uOcclusionTexture, 3);
      }
    } else {
      // bind the texture of the base color
      if (uBaseColorTexture >= 0) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTexture, 0);
      }
      if (uBaseColorFactor >= 0) { // if this material has a base color
        glUniform4f(uBaseColorFactor,1,1,1,1);
      }
      if (uMetallicFactor >= 0) {
        glUniform1f(uMetallicFactor, 1.f);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(uRoughnessFactor, 1.f);
      }
      if (uMetallicRoughnessTexture >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTexture >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uEmissiveTexture, 2);
      }
      if (uOcclusionFactor >= 0) {
        glUniform1f(uOcclusionFactor, 0.f);
      }
      if (uOcclusionTexture >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTexture, 3);
      }

    }
  };


  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // Send light to shader
    if(uLightingDirection >= 0 && uLightIntensity >= 0){
      glUniform3f(uLightIntensity, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
      
      if (uApplyOcclusion >= 0) {
        glUniform1i(uApplyOcclusion, applyOcclusion);
      }

      if (lightFromCamera)
      {
        glUniform3f(uLightingDirection, 0, 0, 1);
      }
      else
      {
        const auto lightDirectionNormalize = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
        glUniform3f(uLightingDirection, lightDirectionNormalize[0], lightDirectionNormalize[1], lightDirectionNormalize[2]);
      }
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          //auto &node = model.scenes[model.defaultScene].nodes[nodeIdx];
          auto &node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

          if(node.mesh >= 0){
            // Compute matrices
            const auto mvMatrix = viewMatrix * modelMatrix;
            const auto mvpMatrix = projMatrix * mvMatrix;
            const auto normalMatrix = glm::transpose(glm::inverse(mvMatrix));

            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvpMatrix));;
            glUniformMatrix4fv( modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(mvMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

            //auto &mesh = meshIndexToVaoRange[node.mesh];
            auto &vaoRange = meshIndexToVaoRange[node.mesh];

            
            for(int primitiveIdx = 0; primitiveIdx < model.meshes[node.mesh].primitives.size(); ++primitiveIdx){
              auto currentVAO = vertexArrayObjects[vaoRange.begin+primitiveIdx];
              //glBindVertexArray(currentVAO);

              auto primitive = model.meshes[node.mesh].primitives[primitiveIdx];
              
              //bindMaterial(primitiveIdx);
              bindMaterial(primitive.material);
              glBindVertexArray(currentVAO);

              if(primitive.indices >= 0){
                const auto &accessor = model.accessors[primitive.indices];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset = accessor.byteOffset + bufferView.byteOffset;

                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid*)byteOffset);
              }else{
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];

                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          
          for(int nodeChildIdx = 0; nodeChildIdx < node.children.size(); ++nodeChildIdx){
            drawNode(nodeChildIdx, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for(int nodeIdx = 0; nodeIdx < model.scenes[model.defaultScene].nodes.size(); ++nodeIdx){
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  // render image if m_OutputPath is not empty
  if(!m_OutputPath.empty()){
    // call renderToImage
    std::vector<unsigned char> pixels(m_nWindowHeight * m_nWindowWidth);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
      drawScene(cameraController->getCamera());
    });

    // flip image
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

    // output the png
    const auto strPath = m_OutputPath.string();
    stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

    return 0; // quit application after rendering
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        // Camera mode radio
        static int cameraMode = 0;
        /*
        if (ImGui::RadioButton("Trackball", cameraMode == 0))
        {
          const auto currentCamera = cameraController->getCamera();
          cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          cameraController->setCamera(currentCamera);
        }
        if (ImGui::RadioButton("First Person", cameraMode == 1))
        {
          const auto currentCamera = cameraController->getCamera();
          cameraController = std::make_unique<FirstPersonCameraController>(m_GLFWHandle.window(), 0.5f * maxDistance);
          cameraController->setCamera(currentCamera);
        }
        */
        const auto cameraControllerTypeChanged =
            ImGui::RadioButton("Trackball", &cameraMode, 0) ||
            ImGui::RadioButton("First Person", &cameraMode, 1);
        if (cameraControllerTypeChanged) {
          const auto currentCamera = cameraController->getCamera();
          if (cameraMode == 0) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          } else {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(currentCamera);
        }

        // Light
        if(ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)){
          static float angle1 = 0.f;
          static float angle2 = 0.f;
          const auto anglesChanged = (ImGui::SliderFloat("angle 1", &angle1, 0.0f, 3.14f) || ImGui::SliderFloat("angle 2", &angle2, 0.0f, 2*3.14f));
          if(anglesChanged){
            lightDirection = glm::vec3(glm::sin(angle1) * glm::cos(angle2), glm::cos(angle1), glm::sin(angle1)*glm::sin(angle2));
          }
          static glm::vec3 lightColorInput(1.f, 1.f, 1.f);
          static float lightIntensityInput = 1.f;
          if(ImGui::ColorEdit3("color", (float *)&lightColorInput) || ImGui::InputFloat("intensity", &lightIntensityInput)){
            lightIntensity = lightColorInput * lightIntensityInput;
          }
          //static bool lightFromCamera;
          ImGui::Checkbox("Light from camera", &lightFromCamera);
          ImGui::Checkbox("Apply occlusion", &applyOcclusion);
        }
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output, const std::string &upVector) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output},
    m_upVector{upVector}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
