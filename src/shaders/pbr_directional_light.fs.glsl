#version 330

in vec3 vViewSpaceNormal;
in vec3 vViewSpacePosition;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform sampler2D uEmissiveTexture;
uniform vec3 uEmissiveFactor;

uniform sampler2D uOcclusionTexture;
uniform vec3 uOcclusionFactor;

uniform int uApplyOcclusion;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{
  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDirection;
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);

  vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));

  vec4 metallicRougnessFromTexture =
      texture(uMetallicRoughnessTexture, vTexCoords);

  //vec3 diffuse = baseColorFromTexture.rgb * M_1_PI;
  vec4 baseColor = baseColorFromTexture * uBaseColorFactor;
  //vec3 diffuse = baseColor.rgb * M_1_PI;
  vec3 metallic = vec3(metallicRougnessFromTexture.b * uMetallicFactor);
  float roughness = metallicRougnessFromTexture.g * uRoughnessFactor;

  float dielectricSpecular = 0.04;
  vec3 black = vec3(0.);

  vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular), black, metallic);
  vec3 F_0 = mix(vec3(0.04), baseColor.rgb, metallic);
  float alpha = roughness * roughness;

  float VdotH = clamp(dot(V, H), 0., 1.);
  float baseShlickFactor = (1 - abs(VdotH));
  float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
  shlickFactor *= shlickFactor; // power 4
  shlickFactor *= baseShlickFactor; // power 5
  vec3 F = F_0 + (1 - F_0) * shlickFactor;

  vec3 f_diffuse = (1 - F) * M_1_PI * c_diff;

  float NdotH = clamp(dot(N, H), 0., 1.);
  float denom = (NdotH * NdotH * ((alpha * alpha) - 1.) + 1.);
  float D = M_1_PI * ((alpha * alpha) + NdotH)/ (denom * denom);
  
  float NdotL = clamp(dot(N, L), 0., 1.);
  float NdotV = clamp(dot(N, V), 0., 1.);
  float visDenominator =
      NdotL * sqrt(NdotV * NdotV * (1 - (alpha*alpha)) + (alpha*alpha)) +
      NdotV * sqrt(NdotL * NdotL * (1 - (alpha*alpha)) + (alpha*alpha));
  float Vis = visDenominator > 0. ? 0.5 / visDenominator : 0.0;

  vec3 f_specular = F * Vis * D;

  vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;
  
  vec3 result = (f_diffuse + f_specular) * uLightIntensity * NdotL + emissive;
  if (uApplyOcclusion == 1) {
    result = mix(result, result * texture2D(uOcclusionTexture, vTexCoords).r, uOcclusionFactor);
  }

  //fColor =  LINEARtoSRGB(diffuse * uLightIntensity * NdotL);
  fColor = LINEARtoSRGB(result);
}