#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightingDirection;
//uniform float lightIntensity;
uniform vec3 uLightIntensity;

out vec3 fColor;

void main()
{
   // Need another normalization because interpolation of vertex attributes does not maintain unit length
   vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
   //fColor = vec3(1, 0, 1);
   float oneOverPi = 1. / 3.14;
   fColor=vec3(oneOverPi, oneOverPi, oneOverPi) * uLightIntensity * dot(viewSpaceNormal, uLightingDirection);
}